import os
import pickle
from abc import ABCMeta, abstractmethod
from functools import partial

import numpy.matlib as npm
from time import strftime, gmtime, time

import numpy as np
from collections import Counter
from typing import Iterable, Callable, List

from scipy.special import logsumexp

START_STATE = '*START*'
START_WORD = '*START*'
END_STATE = '*END*'
END_WORD = '*END*'
RARE_WORD = '*RARE_WORD*'
EPSILON = 1e-20


class Model(metaclass=ABCMeta):
    @abstractmethod
    def MAP(self, sentences) -> Iterable: ...

    @staticmethod
    def get_name() -> str: ...


def zip_consecutive(iterable, n=2):
    iters = []
    for offset in range(n):
        it = iter(iterable)
        for _ in range(offset):
            next(it)
        iters.append(it)

    while True:
        try:
            l = []
            for it in iters:
                l.append(next(it))
            yield tuple(l)
        except (RuntimeError, StopIteration):
            break


def data_example(data_path='PoS_data.pickle',
                 words_path='all_words.pickle',
                 pos_path='all_PoS.pickle'):
    """
    An example function for loading and printing the Parts-of-Speech data for
    this exercise.
    Note that these do not contain the "rare" values and you will need to
    insert them yourself.

    :param data_path: the path of the PoS_data file.
    :param words_path: the path of the all_words file.
    :param pos_path: the path of the all_PoS file.
    """

    with open('PoS_data.pickle', 'rb') as f:
        data = pickle.load(f)
    with open('all_words.pickle', 'rb') as f:
        words = pickle.load(f)
    with open('all_PoS.pickle', 'rb') as f:
        pos = pickle.load(f)

    print("The number of sentences in the data set is: " + str(len(data)))
    print("\nThe tenth sentence in the data set, along with its PoS is:")
    print(data[10][1])
    print(data[10][0])

    print("\nThe number of words in the data set is: " + str(len(words)))
    print("The number of parts of speech in the data set is: " + str(len(pos)))

    print("one of the words is: " + words[34467])
    print("one of the parts of speech is: " + pos[17])

    print(pos)


class Baseline(Model):
    '''
    The baseline model.
    '''

    def __init__(self, pos_tags, words, training_set):
        '''
        The init function of the baseline Model.
        :param pos_tags: the possible hidden states (POS tags)
        :param words: the possible emissions (words).
        :param training_set: A training set of sequences of POS-tags and words.
        '''

        self.words = words + [START_WORD, END_WORD, RARE_WORD]
        self.pos_tags = pos_tags + [START_STATE, END_STATE]
        training_set = preprocess_data(training_set)

        self.words_size = len(self.words)
        self.pos_size = len(self.pos_tags)
        self.pos2i = {pos: i for (i, pos) in enumerate(self.pos_tags)}
        self.word2i = {word: i for (i, word) in enumerate(self.words)}

        self.q, self.e = baseline_mle(training_set, self)
        self.e_weighted = self.e * self.q[:, np.newaxis]

    def __MAP_one(self, sentence):
        pos = []
        for word in sentence:
            pos_index_max_for_word = np.argmax(self.e_weighted[:, self.word2i[word]])
            pos.append(self.pos_tags[pos_index_max_for_word])
        return pos

    def MAP(self, sentences):
        '''
        Given an iterable sequence of word sequences, return the most probable
        assignment of PoS tags for these words.
        :param sentences: iterable sequence of word sequences (sentences).
        :return: iterable sequence of PoS tag sequences.
        '''
        return map(self.__MAP_one, sentences)

    @staticmethod
    def get_name():
        return 'Baseline'


def baseline_mle(training_set, model: Baseline):
    """
    a function for calculating the Maximum Likelihood estimation of the
    multinomial and emission probabilities for the baseline model.

    :param training_set: an iterable sequence of sentences, each containing
            both the words and the PoS tags of the sentence (as in the "data_example" function).
    :param model: an initial baseline model with the pos2i and word2i mappings among other things.
    :return: a mapping of the multinomial and emission probabilities. You may implement
            the probabilities in |PoS| and |PoS|x|Words| sized matrices, or
            any other data structure you prefer.
    """

    e = np.zeros((model.pos_size, model.words_size))

    for sentence in training_set:
        for tag, word in zip(*sentence):
            e[model.pos2i[tag], model.word2i[word]] += 1

    q = np.sum(e, axis=1)
    q /= np.sum(q)

    e /= np.sum(e, axis=1, keepdims=True)

    return q, e


class HMM(Model):
    '''
    The basic HMM_Model with multinomial transition functions.
    '''

    def __init__(self, pos_tags, words, training_set):
        '''
        The init function of the basic HMM Model.
        :param pos_tags: the possible hidden states (POS tags)
        :param words: the possible emissions (words).
        :param training_set: A training set of sequences of POS-tags and words.
        '''

        self.words = words + [START_WORD, END_WORD, RARE_WORD]
        self.pos_tags = pos_tags + [START_STATE, END_STATE]
        training_set = preprocess_data(training_set)

        self.words_size = len(self.words)
        self.pos_size = len(self.pos_tags)
        self.pos2i = {pos: i for (i, pos) in enumerate(self.pos_tags)}
        self.word2i = {word: i for (i, word) in enumerate(self.words)}

        self.t, self.e = hmm_mle(training_set, self)

    def sample_one(self):
        sequence = [START_WORD]
        last_tag = START_STATE

        while last_tag != END_STATE:
            last_tag = self.pos_tags[np.random.choice(self.pos_size, p=self.t[self.pos2i[last_tag], :])]
            word = self.words[np.random.choice(self.words_size, p=self.e[self.pos2i[last_tag], :])]
            sequence.append(word)

        return sequence[1:-1]

    def sample(self, n):
        '''
        Sample n sequences of words from the HMM.
        :return: A list of word sequences.
        '''

        for _ in range(n):
            yield self.sample_one()

    def viterbi_one(self, sentence):
        sentence = [START_WORD] + sentence + [END_WORD]

        log_t = np.log(self.t)
        log_e = np.log(self.e)

        pi = np.zeros((self.pos_size, len(sentence)))
        xi = np.zeros((self.pos_size, len(sentence)), dtype=int)

        pi[:, 0] = log_t[self.pos2i[START_STATE], :] + log_e[:, self.word2i[sentence[0]]]

        for t in range(1, len(sentence)):
            # t is loc in sentence
            x = (pi[:, t - 1, np.newaxis] + log_t + log_e[:, self.word2i[sentence[t]], np.newaxis]).T
            pi[:, t] = np.max(x, axis=1)
            xi[:, t] = np.argmax(x, axis=1)

        pos_idx = np.zeros((len(sentence),), dtype=int)

        # calculate best path
        pos_idx[-1] = np.argmax(pi[:, -1])
        for i in reversed(range(0, len(sentence) - 1)):
            pos_idx[i] = xi[pos_idx[i + 1], i]

        tags = [self.pos_tags[tag] for tag in pos_idx[1:-1]]  # remove START, END by [1:-1]

        return tags

    def viterbi(self, sentences):
        '''
        Given an iterable sequence of word sequences, return the most probable
        assignment of PoS tags for these words.
        :param sentences: iterable sequence of word sequences (sentences).
        :return: iterable sequence of PoS tag sequences.
        '''
        return map(self.viterbi_one, sentences)

    def MAP(self, sentences):
        return self.viterbi(sentences)

    @staticmethod
    def get_name():
        return 'HMM'


def hmm_mle(training_set, model):
    """
    a function for calculating the Maximum Likelihood estimation of the
    transition and emission probabilities for the standard multinomial HMM.

    :param training_set: an iterable sequence of sentences, each containing
            both the words and the PoS tags of the sentence (as in the "data_example" function).
    :param model: an initial HMM with the pos2i and word2i mappings among other things.
    :return: a mapping of the transition and emission probabilities. You may implement
            the probabilities in |PoS|x|PoS| and |PoS|x|Words| sized matrices, or
            any other data structure you prefer.
    """

    t = np.zeros((model.pos_size, model.pos_size))
    e = np.zeros((model.pos_size, model.words_size))

    for tags, sentence in training_set:
        for tag, word in zip(tags, sentence):
            e[model.pos2i[tag], model.word2i[word]] += 1

        for yi, yj in zip_consecutive(tags):
            t[model.pos2i[yi], model.pos2i[yj]] += 1

    # to avoid division by zero
    t[t == 0] = EPSILON
    e[e == 0] = EPSILON

    t /= np.sum(t, axis=1, keepdims=True)
    e /= np.sum(e, axis=1, keepdims=True)

    return t, e


PHI_MAPPING = Callable[[str, str, str], List[int]]


class MEMM(Model):
    '''
    The base Maximum Entropy Markov Model with log-linear transition functions.
    '''

    def __init__(self, pos_tags, words, training_set, phi: PHI_MAPPING):
        '''
        The init function of the MEMM.
        :param pos_tags: the possible hidden states (POS tags)
        :param words: the possible emissions (words).
        :param training_set: A training set of sequences of POS-tags and words.
        :param phi: the feature mapping function, which accepts two PoS tags
                    and a word, and returns a list of indices that have a "1" in
                    the binary feature vector.
        '''

        self.words = words + [START_WORD, END_WORD, RARE_WORD]
        self.pos_tags = pos_tags + [START_STATE, END_STATE]
        training_set = preprocess_data(training_set)

        self.words_size = len(self.words)
        self.pos_size = len(self.pos_tags)
        self.pos2i = {pos: i for (i, pos) in enumerate(self.pos_tags)}
        self.word2i = {word: i for (i, word) in enumerate(self.words)}

        self.phi = phi
        self.phi_dim = 1
        w0 = np.ones(self.phi_dim)
        self.w = perceptron(training_set, self, w0)

    def viterbi_one(self, sentence, w):

        log_p = np.zeros((self.pos_size, len(sentence)))
        xi = np.zeros((self.pos_size, len(sentence)), dtype=int)
        assert sentence[-1] == END_WORD
        assert sentence[0] == START_WORD

        log_p[:, 0] = -np.inf
        log_p[self.pos2i[START_STATE], 0] = 0
        xi[self.pos2i[START_STATE], 0] = 44
        for t in range(1, len(sentence)):
            # t is loc in sentence
            indices = np.array([
                [
                    self.phi(prev_pos, cur_pos, sentence[t])
                    for cur_pos in self.pos_tags
                ]
                for prev_pos in self.pos_tags
            ])
            w = np.resize(w, max(np.max(indices[:, :, -1]), *w.shape) + 1)
            score = np.sum(w[indices], axis=2)

            z = logsumexp(score, axis=0, keepdims=False)
            for i in range(self.pos_size):
                v = log_p[:, t - 1] + score[:, i] - z
                log_p[i, t] = np.max(v)
                xi[i, t] = np.argmax(v)

        pos_idx = np.zeros((len(sentence),), dtype=int)
        pos_idx[-1] = np.argmax(log_p[:, -1])
        for i in range(1, len(sentence)):
            pos_idx[-1 - i] = xi[pos_idx[-i], -i]

        return [self.pos_tags[tag] for tag in pos_idx]

    def viterbi(self, sentences, w):
        '''
        Given an iterable sequence of word sequences, return the most probable
        assignment of POS tags for these words.
        :param sentences: iterable sequence of word sequences (sentences).
        :param w: a dictionary that maps a feature index to it's weight.
        :return: iterable sequence of POS tag sequences.
        '''

        return map(partial(self.viterbi_one, w=w), sentences)

    def MAP(self, sentences):
        return self.viterbi(sentences, self.w)


def perceptron(training_set, initial_model: MEMM, w0, eta=0.1, epochs=1, verbose=True):
    """
    learn the weight vector of a log-linear model according to the training set.
    :param training_set: iterable sequence of sentences and their parts-of-speech.
    :param initial_model: an initial MEMM object, containing among other things
            the phi feature mapping function.
    :param w0: an initial weights vector.
    :param eta: the learning rate for the perceptron algorithm.
    :param epochs: the amount of times to go over the entire training data (default is 1).
    :return: w, the learned weights vector for the MEMM.
    """

    w = w0
    learn = 0
    print_each = 0.0001
    cumulative_percent = 0
    for epoch in range(epochs):

        for tag, sentence in training_set:
            if verbose and (learn / len(training_set)) >= print_each:
                cumulative_percent += (learn / len(training_set)) * 100
                print(f'\rlearned {cumulative_percent:.2f}%', end='')
                learn = 0

            predict = initial_model.viterbi_one(sentence, w)

            for i, j in zip_consecutive(range(len(tag))):
                left_sum_indicas = initial_model.phi(tag[i], tag[j], sentence[j])
                right_sum_indices = initial_model.phi(predict[i], predict[j], sentence[j])

                # w size is unknown at first
                max_index = max(np.max(left_sum_indicas), np.max(right_sum_indices))
                if len(w) < max_index:
                    w = np.resize(w, max_index + 1)

                w[left_sum_indicas] += eta
                w[right_sum_indices] -= eta
            learn += 1

    if verbose:
        print()
    return w


def remove_rare_words(words, data, threshold=3):
    word_counter = Counter(word for (tags, sentence) in data for word in sentence)

    def replace_with_rare(sentence):
        return sentence[0], [w if word_counter[w] > threshold else RARE_WORD for t, w in zip(*sentence)]

    data = list(map(replace_with_rare, data))
    words = list(filter(lambda w: word_counter[w] > threshold, words))

    return words, data


def preprocess_data(data):
    def add_start_end(sentence):
        return [START_STATE] + sentence[0] + [END_STATE], [START_WORD] + sentence[1] + [END_WORD]

    return list(map(add_start_end, data))


def test_model(model: Model, test_data):
    test_pos, test_sentences = zip(*test_data)
    prediction_it = model.MAP(test_sentences)
    real_it = iter(test_pos)
    print(f'predict: {next(prediction_it)}', f'real   : {next(real_it)}',
          sep='\n', end=f'\n{"*"*50}\n')


def evaluate_model(model: Model, test_data) -> float:
    test_data = preprocess_data(test_data)
    test_pos, test_sentences = zip(*test_data)
    prediction_it = model.MAP(test_sentences)
    real_it = iter(test_pos)

    mismatches = 0
    all_count = 0
    for real, prediction in zip(real_it, prediction_it):
        assert len(real) == len(prediction)
        mismatches += sum(x != y for x, y in zip(real, prediction))
        all_count += len(real)
        # print(f'{mismatches / all_count * 100} error')

    return mismatches / all_count * 100


def simple_phi(pos_tags, words):
    words = words + [START_WORD, END_WORD, RARE_WORD]
    pos_tags = pos_tags + [START_STATE, END_STATE]

    pos_size = len(pos_tags)
    words_size = len(words)
    pos2i = {pos: i for (i, pos) in enumerate(pos_tags)}
    word2i = {word: i for (i, word) in enumerate(words)}

    def simple_phi(prev_tag, cur_tag, word):
        indices = []

        # transition
        indices.append(pos2i[prev_tag] * pos_size + pos2i[cur_tag])

        # emission
        emissions_start_index = pos_size * pos_size
        indices.append(emissions_start_index + pos2i[cur_tag] * words_size + word2i[word])

        return indices

    return simple_phi


def capitals_phi(pos_tags, words) -> PHI_MAPPING:
    words = words + [START_WORD, END_WORD, RARE_WORD]
    pos_tags = pos_tags + [START_STATE, END_STATE]

    pos_size = len(pos_tags)
    words_size = len(words)
    pos2i = {pos: i for (i, pos) in enumerate(pos_tags)}
    word2i = {word: i for (i, word) in enumerate(words)}

    def capitals_phi(prev_tag, cur_tag, word):
        indices = []

        # Transition
        indices.append(pos2i[prev_tag] * pos_size + pos2i[cur_tag])

        # Emission
        emissions_start_index = pos_size * pos_size
        indices.append(emissions_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # Capital
        capital_start_index = emissions_start_index + pos_size * words_size
        if word[0].isupper():
            indices.append(capital_start_index + pos2i[cur_tag] * words_size + word2i[word])

        return indices

    return capitals_phi


def pronoun_phi(pos_tags, words) -> PHI_MAPPING:
    words = words + [START_WORD, END_WORD, RARE_WORD]
    pos_tags = pos_tags + [START_STATE, END_STATE]

    pos_size = len(pos_tags)
    words_size = len(words)
    pos2i = {pos: i for (i, pos) in enumerate(pos_tags)}
    word2i = {word: i for (i, word) in enumerate(words)}

    pronouns = {'I', 'me', 'we', 'us', 'you', 'she', 'her', 'he', 'him', 'it', 'they', 'them',
                'that', 'which', 'who', 'whom', 'whose', 'whichever', 'whoever', 'whomever',
                'this', 'that', 'these', 'those'}
    pronouns |= set(map(lambda w: w[0].upper() + w[1:], pronouns))

    def pronoun_phi(prev_tag, cur_tag, word):
        indices = []

        # Transition
        indices.append(pos2i[prev_tag] * pos_size + pos2i[cur_tag])

        # Emission
        emissions_start_index = pos_size * pos_size
        indices.append(emissions_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # Pronouns
        pronouns_start_index = emissions_start_index + pos_size * words_size
        if word in pronouns:
            indices.append(pronouns_start_index + pos2i[cur_tag] * words_size + word2i[word])

        return indices

    return pronoun_phi


def simple_past_phi(pos_tags, words) -> PHI_MAPPING:
    words = words + [START_WORD, END_WORD, RARE_WORD]
    pos_tags = pos_tags + [START_STATE, END_STATE]

    pos_size = len(pos_tags)
    words_size = len(words)
    pos2i = {pos: i for (i, pos) in enumerate(pos_tags)}
    word2i = {word: i for (i, word) in enumerate(words)}

    def past_phi(prev_tag, cur_tag, word):
        indices = []

        # Transition
        indices.append(pos2i[prev_tag] * pos_size + pos2i[cur_tag])

        # Emission
        emissions_start_index = pos_size * pos_size
        indices.append(emissions_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # past
        past_start_index = emissions_start_index + pos_size * words_size
        if word.endswith('ed'):
            indices.append(past_start_index + pos2i[cur_tag] * words_size + word2i[word])

        return indices

    return past_phi


def combined_phi(pos_tags, words) -> PHI_MAPPING:
    words = words + [START_WORD, END_WORD, RARE_WORD]
    pos_tags = pos_tags + [START_STATE, END_STATE]

    pos_size = len(pos_tags)
    words_size = len(words)
    pos2i = {pos: i for (i, pos) in enumerate(pos_tags)}
    word2i = {word: i for (i, word) in enumerate(words)}

    pronouns = {'I', 'me', 'we', 'us', 'you', 'she', 'her', 'he', 'him', 'it', 'they', 'them',
                'that', 'which', 'who', 'whom', 'whose', 'whichever', 'whoever', 'whomever',
                'this', 'that', 'these', 'those'}
    pronouns |= set(map(lambda w: w[0].upper() + w[1:], pronouns))

    def combined_phi(prev_tag, cur_tag, word):
        indices = []
        # Transition
        indices.append(pos2i[prev_tag] * pos_size + pos2i[cur_tag])

        # Emission
        emissions_start_index = pos_size * pos_size
        indices.append(emissions_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # Capital
        capital_start_index = emissions_start_index + pos_size * words_size
        if word[0].isUpper():
            indices.append(capital_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # past
        past_start_index = capital_start_index + pos_size * words_size
        if word.endswith('ed'):
            indices.append(past_start_index + pos2i[cur_tag] * words_size + word2i[word])

        # Pronouns
        pronouns_start_index = past_start_index + pos_size * words_size
        if word in pronouns:
            indices.append(pronouns_start_index + pos2i[cur_tag] * words_size + word2i[word])

        return indices

    return combined_phi


def evaluate(model_class: type, train_percents, train_data, test_data, pos, words, write=False):
    name = model_class.get_name()
    with open(os.path.join('out', f'{name}_{train_percents}.sum'), 'w') as res_file:
        line = (f'Training {name} model with {train_percents}% of the data. '
                f'started at {strftime("%Y-%m-%d %H:%M:%S", gmtime())}')
        print(line)
        if write:
            res_file.write(f'{line}\n')
            res_file.flush()

        start = time()
        model = model_class(pos, words, train_data)
        elapsed_time = time() - start

        line = (f'finished training at {strftime("%Y-%m-%d %H:%M:%S", gmtime())}. '
                f'took {elapsed_time}\n'
                f'evaluating...')
        print(line)
        if write:
            res_file.write(f'{line}\n')
            res_file.flush()

        start = time()
        acc = evaluate_model(model, test_data)
        elapsed_time = time() - start

        line = (f'{name} model, trained with {train_percents}% of the data (of size {len(test_data)}).\n'
                f'took {elapsed_time}\n'
                f'Model error percent is: {acc} '
                f'time now {strftime("%Y-%m-%d %H:%M:%S", gmtime())}')
        print(line)
        if write:
            res_file.write(f'{line}\n')
            res_file.flush()


def embed_phi_to_memm_class(phi):
    class MEMM_(MEMM):
        def __init__(self, pos_tags, words, training_set):
            super().__init__(pos_tags, words, training_set, phi)

        @staticmethod
        def get_name():
            return f'MEMM_{phi.__name__}'

    return MEMM_


def main():
    with open('PoS_data.pickle', 'rb') as f:
        data = pickle.load(f)
    with open('all_words.pickle', 'rb') as f:
        words = pickle.load(f)
    with open('all_PoS.pickle', 'rb') as f:
        pos = pickle.load(f)

    words, data = remove_rare_words(words, data)
    np.random.shuffle(data)

    test_percent = 10
    test_data = data[:int(test_percent / 100 * len(data))]

    np.random.shuffle(data)

    for cls in [
                embed_phi_to_memm_class(capitals_phi(pos, words)),
                embed_phi_to_memm_class(simple_past_phi(pos, words)),
                embed_phi_to_memm_class(pronoun_phi(pos, words)),
                embed_phi_to_memm_class(combined_phi(pos, words))]:
        for train_percents in [10, 25, 90]:
            train_data = data[:int(train_percents / 100 * len(data))]
            evaluate(cls, train_percents, train_data, test_data, pos, words)


if __name__ == '__main__':
    main()
