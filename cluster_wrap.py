#!/usr/bin/python3
#SBATCH --time=10:03:00
#SBATCH --output=job_%j.out
import signal
import sys
from time import strftime, gmtime, sleep
import importlib.util

path_mod = "/cs/usr/elazar.cohen1/Desktop/apml-ex2/PoS Tagging.py"

spec = importlib.util.spec_from_file_location("PoS Tagging", path_mod)
PoS_Tagging = importlib.util.module_from_spec(spec)
spec.loader.exec_module(PoS_Tagging)


# define the handler function
# note that this is not executed here, but rather
# when the associated signal is sent

def term_handler(signum, frame):
    print("TERM signal handler called.  Exiting.")
    # do other stuff to cleanup here
    exit(-1)

# associate the function "term_handler" with the TERM signal
signal.signal(signal.SIGTERM, term_handler)

# Do your normal work here

PoS_Tagging.main()

